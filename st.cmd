require busy
require adcore,3.10.0+1-ifc
require essioc
require admisc,2.1.2-ifc
require adifc14,0.0.1+0

# epicsEnvSet("EPICS_CA_ADDR_LIST", "172.16.62.8 172.16.62.19 172.16.61.255 172.16.62.18")
# epicsEnvSet("EPICS_PVA_ADDR_LIST", "172.16.62.8 172.16.62.19 172.16.61.255 172.16.62.18")
# epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
# epicsEnvSet("EPICS_PVA_AUTO_ADDR_LIST", "NO")

errlogInit(20000)
callbackSetQueueSize(30000)

###############################################################################
# - IOC Instance parameters (should come from CCDB + IOC Factory)
###############################################################################
epicsEnvSet("CONTROL_GROUP", "PBI-EMU02")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-120")
epicsEnvSet("AMC_DEVICE", "0")
epicsEnvSet("EVR_NAME", "Ctrl-EVR-101:")
###############################################################################

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("PORT",                         "$(AMC_NAME)")
epicsEnvSet("MAX_SAMPLES",                  "2048")
#- AD plugin macros
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

epicsEnvSet("ACQ_DEV",       "$(ACQ_DEVID)")
epicsEnvSet("ACQ_PREFIX",    "$(RACKROW):$(ACQ_UNIT):")
epicsEnvSet("ACQ_PORT",      "AMC.$(ACQ_DEVID)")
epicsEnvSet("XSIZE",         "$(ACQ_SAMPLES)")
epicsEnvSet("YSIZE",         "1")
epicsEnvSet("QSIZE",         "20")
epicsEnvSet("NCHANS",        "100")
epicsEnvSet("CBUFFS",        "500")
epicsEnvSet("MAX_THREADS",   "4")


# Create a IFC14xx driver
# ifc1400Configure(const char *portName, const int ifccard, const int numChannels,
#        int numSamples, int extraPorts, int maxBuffers, int maxMemory,
#        int priority, int stackSize)
ifc1400Configure("$(PORT)", $(AMC_DEVICE), 40, $(MAX_SAMPLES), 0, 0)
dbLoadRecords("$(adifc14_DB)/ifc1400.template","P=$(PREFIX),R=,PORT=$(PORT),MAX_SAMPLES=$(MAX_SAMPLES)")
dbLoadRecords("$(adifc14_DB)/ifc1400-evr.template","P=$(PREFIX),R=,PORT=$(PORT),EVR_DEV=$(CONTROL_GROUP):$(EVR_NAME)")

# #- 10x data channels
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=0, NAME=CH1")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=1, NAME=CH2")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=2, NAME=CH3")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=3, NAME=CH4")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=4, NAME=CH5")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=5, NAME=CH6")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=6, NAME=CH7")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=7, NAME=CH8")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=8, NAME=CH9")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=9, NAME=CH10")

iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=10, NAME=CH11")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=11, NAME=CH12")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=12, NAME=CH13")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=13, NAME=CH14")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=14, NAME=CH15")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=15, NAME=CH16")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=16, NAME=CH17")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=17, NAME=CH18")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=18, NAME=CH19")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=19, NAME=CH20")

iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=20, NAME=CH21")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=21, NAME=CH22")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=22, NAME=CH23")
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=23, NAME=CH24")

#- Fanout records for areadetector params across all 24 channels
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=ROI2-MinX")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=ROI2-SizeX")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=ROI2-EndX, INIVAL=2048")

dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-EnableOffsetScale, INIVAL=1")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-Scale, INIVAL=1")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-Offset, INIVAL=0")

dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-EnableFilter, INIVAL=1")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-NumFilter, INIVAL=14")
dbLoadRecords("$(adifc14_DB)/mebtemu-fanout.template", "P=$(PREFIX), R=, PLUGINPARAM=PROC2-FilterType, INIVAL=1")

#- Profile waveform (each element one wire)
epicsEnvSet("TSSOURCE", "$(CONTROL_GROUP):$(EVR_NAME)F14HzCnt-I.TIME CA")
dbLoadRecords("$(E3_CMD_TOP)/db/mebt-emu-profile.template", "P=$(CONTROL_GROUP):,R=:,DAQPREFIX=$(PREFIX),HV=V,TSEL=$(TSSOURCE)")

#- Force processing of the offset compensation
dbLoadRecords("$(E3_CMD_TOP)/db/offset-comp-fanout.template", "P=$(PREFIX),R=")

set_pass1_restoreFile("$(E3_CMD_TOP)/default_settings.sav", "P=$(PREFIX),R=")

epicsEnvSet "ENGINEER" "Rafael Baron"
epicsEnvSet "IOCNAME" "PBI-EMU02:SC-IOC-003"
epicsEnvSet "IOCDIR" "PBI-EMU02_SC-IOC-003"
epicsEnvSet "LOG_SERVER_NAME" "172.16.107.59"
epicsEnvSet "AS_TOP" "$(E3_CMD_TOP)/autosave"
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

#- run IOC
iocInit

#- Channel access links are not working on ppc64 architecture from beginning
dbpf $(PREFIX)#EvrTimestampProc.PROC 1
dbpf $(PREFIX)#EvrTimestampProc.INPA "$(CONTROL_GROUP):$(EVR_NAME)F14HzCnt-I CPP"
